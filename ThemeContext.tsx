import React from 'react';
import { View } from 'react-native';
import {
  DefaultTheme,
  DarkTheme
} from '@react-navigation/native';
import generatedStyleSheet from "./styles";

// Make sure the shape of the default value passed to
// createContext matches the shape that the consumers expect!
export const ThemeContext = React.createContext({
  theme: DefaultTheme,
  setTheme: () => {},
  toggleTheme: () => {},
  setThemeByName: (themeName) => {},
});

export const ThemeConsumer = props => {
    return <ThemeContext.Consumer>
      {({ theme, toggleTheme, setTheme, setThemeByName }) => {


        const themeSpecificStyles = { 
            backgroundColor: theme.colors.background, 
            textColor: theme.colors.text, 
            thingColor: theme.colors.notification, 
            thingBorderColor: theme.colors.border 
         };
       
        const styles = generatedStyleSheet(themeSpecificStyles);
    	
    	return <View style={styles.container}>{props.render(styles, setThemeByName)}</View>;
    	}}
	</ThemeContext.Consumer>
};