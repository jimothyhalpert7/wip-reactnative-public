/* StorageHelper.tsx */
import AsyncStorage from '@react-native-async-storage/async-storage';

export async function getData() {
  try {
    const jsonValue = await AsyncStorage.getItem('@things')
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch(e) {
    console.log(e);
  }
}

export async function storeData(value) {
  try {
    const jsonValue = JSON.stringify(value)
    await AsyncStorage.setItem('@things', jsonValue)
  } catch (e) {
    console.log(e);
  }
}

