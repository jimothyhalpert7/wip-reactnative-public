import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import { Text, TouchableOpacity } from 'react-native';
import {
  NavigationContainer,
  // DarkTheme,
  // LightTheme,
  Theme,
} from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import ThingScreen from "./ThingScreen";
import HomeScreen from "./HomeScreen";
import { ThemeContext } from "./ThemeContext";
import styles from "./styles";
import OptionsMenu from "react-native-option-menu";
import themes from "./themes.json"


const Stack = createStackNavigator();

function App() {
  // const [count, setCount] = useState(0);
  // const tstFun = () => {
  //   setCount(c => c + 1);
  //   console.log('app lvl fun');
  // };

  const DarkTheme = themes.DarkTheme;
  const LightTheme = themes.LightTheme;

  const [theme, setTheme] = React.useState<Theme>(DarkTheme);

  const setThemeByName = (themeName: string) => {
    setTheme(() => (themeName === 'light' ? LightTheme : DarkTheme));
  }

  const toggleTheme = () => {
    setTheme(() => (theme === DarkTheme ? LightTheme : DarkTheme));
  };

  const screenOptions = {
    title: "",
    headerTransparent: true,
  };

  return (
    <ThemeContext.Provider
      value={{ theme: theme, toggleTheme: toggleTheme, setTheme: setTheme, setThemeByName: setThemeByName }}
    >
      <NavigationContainer theme={theme}>
        <Stack.Navigator screenOptions={screenOptions}>
          <Stack.Screen
            name="Home"
            component={HomeScreen}
            options={{ 
                      headerRight: () => (
            <TouchableOpacity><Text       style={{
                color: theme.colors.text,
                fontSize: 24,
                fontWeight: "bold",
                paddingRight: 18,
              }}>=</Text></TouchableOpacity>
          ),
          }}
          />
          <Stack.Screen name="ThingScreen" component={ThingScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </ThemeContext.Provider>
  );
}

export default App;