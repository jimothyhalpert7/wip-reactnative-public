import React, { useState } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
// import themesFile from "./themes.json";
import generatedStyleSheet from "./styles";
import { ThemeContext, ThemeConsumer, TestComponent } from "./ThemeContext";
import {
  Text,
  View,
  TouchableOpacity,
  Button,
  FlatList,
  Switch,
} from "react-native";
import SegmentedControl from "@react-native-community/segmented-control";
import { DarkTheme, DefaultTheme } from "@react-navigation/native";

const buildDate = new Date().toString();

const getData = async () => {
  try {
    const jsonValue = await AsyncStorage.getItem("@things");
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch (e) {
    console.log(e);
  }
};

const storeData = async (value) => {
  try {
    const jsonValue = JSON.stringify(value);
    await AsyncStorage.setItem("@things", jsonValue);
  } catch (e) {
    console.log(e);
  }
};

function HomeScreen({ route, navigation }) {
  // console.log(JSON.stringify(route))
  // console.log(JSON.stringify(navigation))

  const [devMode, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);

  const clearAll = async () => {
    try {
      await AsyncStorage.clear();
    } catch (e) {
      console.log("Error loading DB");
    }
    console.log("Done.");
  };

  const startingThings = [];
  const [savedThings, setSavedThings] = useState(startingThings);

  getData().then((thingsSavedObject) => {
    if (thingsSavedObject && thingsSavedObject.things.length > 0) {
      setSavedThings(thingsSavedObject.things);
    } else {
      storeData({ things: startingThings });
      setSavedThings(startingThings);
    }
  });


  const { theme, setTheme, toggleTheme, setThemeByName }  = React.useContext(ThemeContext);
  const [currentThemeIndex, setThemeIndex] = useState(0);
  const onSelectTheme = (themeIndex) => {
    setThemeIndex(themeIndex);
    // onThemeChange(themeIndex);
    let themeName = themeIndex === 0 ? 'dark' : 'light';
    setThemeByName(themeName);
  };

  return (
    <ThemeConsumer
      render={(styles, setTheme) => (
        <View>
          <View>
            {savedThings.map((savedThing) => (
              <TouchableOpacity key={savedThing.id}>
                <Text
                  style={{ ...styles.text, fontSize: 26, textAlign: 'center' }}
                  onPress={() =>
                    navigation.navigate("ThingScreen", { ...savedThing, devMode })
                  }
                >
                  {savedThing.name}
                </Text>
              </TouchableOpacity>
            ))}
          </View>
          <SegmentedControl
            values={["Dark", "Light"]}
            selectedIndex={currentThemeIndex}
            onChange={(event) =>
              onSelectTheme(event.nativeEvent.selectedSegmentIndex)
            }
          />
          <Switch
            trackColor={{ false: "#767577", true: "#81b0ff" }}
            thumbColor={devMode ? "#f5dd4b" : "#f4f3f4"}
            ios_backgroundColor="#3e3e3e"
            onValueChange={toggleSwitch}
            value={devMode}
          />
          {devMode && (
            <View>
              <Text>{buildDate}</Text>
              <Text>{JSON.stringify(savedThings)}</Text>
              <Button title="Clear DB" onPress={clearAll} />
            </View>
          )}
        </View>
      )}
    />
  );
}

export default HomeScreen;