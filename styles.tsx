import { StyleSheet } from 'react-native';

const styles = (props) => StyleSheet.create({
 container: {
    flex: 1,
    backgroundColor: props.backgroundColor || '#fff',
    justifyContent: 'center',
  },
  thingsContainer:{
    flex: 1,
    flexDirection: 'row', 
    alignItems: 'center',
    justifyContent: 'space-evenly',    
  },
  thing: {
    color: 'black',  
  },
  bottomButtons: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  button: {
    height: 64,
    width: 64,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: 42,
    fontWeight: 'bold',
    color: props.textColor || 'black',
  },
  textBox: {
    width: 100,
    alignItems: 'center',    
  },
  over: {
    fontSize: 18,
    fontWeight: 'bold',    
    alignItems: 'center',
  },
  text: {
  	color: props.textColor || 'black',
  },
  textInput: { 
  	height: 40, 
  	borderColor: 'gray', 
  	borderWidth: 1 
  },
});

export default styles;